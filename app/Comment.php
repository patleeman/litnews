<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    use SoftDeletes;

    protected $fillable = [
      'item_id',
      'user_id',
      'parent_id',
      'comment'
    ];

    protected $appends = [
      'username',
      'votes',
      'children',
      'user_vote',
      'is_saved'
    ];

    protected $hidden = [
      'user_id',
      'deleted_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    function item ()
    {
      return $this->belongsTo('App\Item');
    }

    function user ()
    {
      return $this->belongsTo('App\User');
    }

    function votes ()
    {
      return $this->hasMany('App\Vote', 'comment_id');
    }

    function children ()
    {
      return $this->hasMany('App\Comment', 'parent_id');
    }

    function parent ()
    {
      return $this->belongsTo('App\Comment', 'parent_id');
    }

    function getChildrenAttribute ()
    {
      return $this->children()->get();
    }

    function getVotesAttribute ()
    {
      $votes = $this->votes()->get();
      $vote_count = 0;
      foreach ($votes as $vote) {
        $vote_count += $vote->vote;
      }
      return $vote_count;
    }

    function getUsernameAttribute ()
    {
      return $this->user()->first()->name;
    }

    function getUserVoteAttribute ()
    {
      if (Auth::check()) {
        $current_user = Auth::user();
        $vote = $this->votes()->where('user_id', $current_user->id)->first();
        if (!is_null($vote)) {
          return $vote->vote;
        } else {
          return null;
        }
      } else {
        return null;
      }
    }

    function getIsSavedAttribute ()
    {
      if (Auth::check()) {
        $saved = \App\Saved::where('user_id', Auth::user()->id)->where('comment_id', $this->id)->get();
        return count($saved) > 0;
      } else {
        return null;
      }
    }
}
