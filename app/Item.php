<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Scout\Searchable;

class Item extends Model
{

  use SoftDeletes, Searchable;

  /**
   * The attributes that should be mutated to dates.
   *
   * @var array
   */
  protected $dates = ['deleted_at'];

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
      'title',
      'url',
      'text',
      'user_id'
  ];

  protected $appends = [
      'username',
      'points',
      'comments',
      'num_comments',
      'user_vote',
      'is_saved'
  ];

  protected $hidden = [
    'deleted_at',
    'updated_at',
    'user_id'
  ];

  public function user ()
  {
    return $this->belongsTo('App\User');
  }

  public function votes ()
  {
    return $this->hasMany('App\Vote');
  }

  // Score = (P-1) / (T+2)^G
  //
  // where,
  // P = points of an item (and -1 is to negate submitters vote)
  // T = time since submission (in hours)
  // G = Gravity, defaults to 1.8 in news.arc
  public function score ()
  {
    $points = 0;
    $votes = $this->votes()->get();
    foreach ($votes as $vote) {
      $points += $vote->vote;
    }

    $submission_time = $this->created_at;
    $hours_since = $submission_time->diffInHours(\Carbon\Carbon::now());
    $score = ($points - 1) / pow(($hours_since + 2), 1.8);
    return $score;
  }

  public function comments ()
  {
    return $this->hasMany('App\Comment', 'item_id');
  }

  public function getPointsAttribute ()
  {
    $votes = $this->votes()->get();
    $vote_count = 0;
    foreach ($votes as $vote) {
      $vote_count += $vote->vote;
    }
    return $vote_count;
  }

  public function getScoreAttribute ()
  {
    return $this->score();
  }

  public function getCommentsAttribute ()
  {
    $comments = $this->comments()
      ->where('parent_id', null)
      ->get()
      ->toArray();

    // Sort by votes
    usort($comments, function ($item1, $item2) {
        return $item2['votes'] <=> $item1['votes'];
    });

    return $comments;
  }

  public function getNumCommentsAttribute ()
  {
    return count($this->comments()->get());
  }

  public function getUsernameAttribute ()
  {
    return $this->user()->first()->name;
  }

  public function toSearchableArray ()
  {
    return [
      'title' => $this->title,
      'url' => $this->url,
      'text' => $this->text
    ];
  }

  function getUserVoteAttribute ()
  {
    if (Auth::check()) {
      $current_user = Auth::user();
      $vote = $this->votes()->where('user_id', $current_user->id)->first();
      if (!is_null($vote)) {
        return $vote->vote;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  function getIsSavedAttribute ()
  {
    if (Auth::check()) {
      $saved = \App\Saved::where('user_id', Auth::user()->id)->where('item_id', $this->id)->get();
      return count($saved) > 0;
    } else {
      return null;
    }
  }

}
