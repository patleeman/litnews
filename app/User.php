<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'twitter', 'about'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'confirmed', 'confirmation_code', 'updated_at', 'role'
    ];

    protected $appends = [
      'karma',
    ];

    function comments ()
    {
      return $this->hasMany('App\Comment');
    }

    function items ()
    {
      return $this->hasMany('App\Item');
    }

    function votes ()
    {
      return $this->hasMany('App\Vote');
    }

    /**
     * Get sum of votes for user's content.
     * @return [type] [description]
     */
    function karma ()
    {
      $karma = 0;
      $comments = $this->comments()->get();
      $links = $this->comments()->get();

      foreach ($comments as $comment) {
        foreach ($comment->votes()->get() as $vote) {
          $karma += $vote->vote;
        }
      }

      foreach ($links as $link) {
        foreach ($link->votes()->get() as $vote) {
          $karma += $vote->vote;
        }
      }
      // return 101;
      return $karma;
    }

    function getKarmaAttribute ()
    {
      return $this->karma();
    }
}
