<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Flag extends Model
{
  protected $fillable = [
    'user_id',
    'item_id',
    'comment_id',
    'message'
  ];

  function user ()
  {
    return $this->belongsTo('App\User');
  }

  function item ()
  {
    return $this->belongsTo('App\Item');
  }

  function comment ()
  {
    return $this->belongsTo('App\Comment');
  }
}
