<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Saved extends Model
{

  protected $appends = [
    'item',
    'comment'
  ];

  protected $fillable = [
    'user_id',
    'item_id',
    'comment_id',
  ];

  protected $hidden = [
    'updated_at',
    'item_id',
    'comment_id',
    'user_id'
  ];

  function user ()
  {
    return $this->belongsTo('App\User');
  }

  function item ()
  {
    return $this->belongsTo('App\Item');
  }

  function comment ()
  {
    return $this->belongsTo('App\Comment');
  }

  function getUserAttribute ()
  {
    return $this->user()->first();
  }

  function getItemAttribute ()
  {
    return $this->item()->first();
  }

  function getCommentAttribute ()
  {
    return $this->comment()->first();
  }

}
