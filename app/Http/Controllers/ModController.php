<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Mail};

class ModController extends Controller
{
    public function flag (Request $request)
    {
        if (Auth::check()) {

          $this->validate($request, [
            'item_id' => 'required_without:comment_id|integer',
            'comment_id' => 'required_without:item_id|integer',
            'message' => 'required|string'
          ]);

          $user = Auth::user();

          $message = $request->input('message');
          $comment_id = $request->input('comment_id');
          $item_id = $request->input('item_id');

          $payload = [
            'message' => $message,
            'user_id' => $user->id
          ];

          $email = "";
          $email .= "{$user->name} has flagged an item on Lit News.\n\n";
          $email .= "Reason for flagging: {$message}\n\n";

          if (!is_null($comment_id) && $comment_id) {
            $comment = \App\Comment::where('id', $comment_id)->first();
            $email .= "Flagged Comment:\n";
            $email .= "Id: {$comment['id']}\n";
            $email .= "Title: {$comment['title']}\n";
            $email .= "Poster: {$comment['username']}\n";
            $payload['comment_id'] = $comment->id;
          }

          if (!is_null($item_id) && $item_id) {
            $item = \App\Item::where('id', $item_id)->first();
            $email .= "Flagged Comment:\n";
            $email .= "Id: {$item['id']}\n";
            $email .= "Title: {$item['title']}\n";
            $email .= "Poster: {$item['username']}\n";
            $payload['item_id'] = $item->id;
          }

          \App\Flag::create($payload);

          Mail::raw($email, function ($message){
           $message->to('patleeman@gmail.com');
           $message->from(Auth::user()->email);
           $message->subject('Lit-news.com Moderator Action');
          });

          return response(200);
        } else {
          return response(401);
        }
    }
}
