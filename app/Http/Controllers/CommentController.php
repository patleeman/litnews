<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    public function getItem ($id, Request $request)
    {
      $comment = \App\Comment::where('id', $id)->with('item')->first();
      return response()->json($comment);
    }

    public function addComment (Request $request)
    {
      if (Auth::check()) {

        $this->validate($request, [
          'item_id' => 'required|numeric',
          'parent_id' => 'nullable|numeric',
          'comment' => 'required|string'
        ]);

        $user = Auth::user();

        $new_comment = \App\Comment::create([
          'user_id' => $user->id,
          'item_id' => $request->input('item_id'),
          'parent_id' => $request->input('parent_id'),
          'comment' => $request->input('comment')
        ]);

        \App\Vote::firstOrCreate([
          'comment_id' => $new_comment->id,
          'user_id' => $user->id,
          'vote' => 1
        ]);

        return response()->json($new_comment);
      } else {
        return response(401);
      }
    }
}
