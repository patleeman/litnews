<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SavedController extends Controller
{
    public function get ()
    {
      if (Auth::check()) {
        $user = Auth::user();
        $saved = \App\Saved::where('user_id', $user->id)->get();
        return response()->json($saved);
      } else {
        return response(401);
      }
    }

    public function save (Request $request)
    {
      if (Auth::check()) {
        $this->validate($request, [
          'item_id' => 'required_without:comment_id',
          'comment_id' => 'required_without:item_id',
        ]);

        $user = Auth::user();

        $payload = [
          'item_id' => $request->input('item_id'),
          'comment_id' => $request->input('comment_id'),
          'user_id' => $user->id
        ];

        $created = \App\Saved::firstOrCreate($payload);

        return response()->json($created);
      } else {
        return response(401);
      }
    }

    public function unsave (Request $request)
    {
      if (Auth::check()) {
        $this->validate($request, [
          'item_id' => 'required_without:comment_id',
          'comment_id' => 'required_without:item_id',
        ]);

        $user = Auth::user();

        if ($request->input('item_id')) {
          $deleted = \App\Saved::where('user_id', $user->id)
                        ->where('item_id', $request->input('item_id'))
                        ->delete();
        } else if ($request->input('comment_id')) {
          $deleted = \App\Saved::where('user_id', $user->id)
                        ->where('comment_id', $request->input('comment_id'))
                        ->delete();
        }

        return response()->json($deleted);
      }
    }



}
