<?php

namespace App\Http\Controllers\Auth;

use Carbon\Carbon;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\{Auth, Validator, Mail};
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
      $confirmation_code = str_random(32);

      $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => bcrypt($data['password']),
          'confirmation_code' => $confirmation_code,
      ]);

      // Send confirmation code email
      // Mail::send('emails.verify', ['confirmation_code' => $confirmation_code], function ($message) use ($data) {
      //   $message->to($data['email'], $data['name'])
      //           ->subject('Please verify your email address');
      // });

      return $user->fresh();
    }

    public function register(Request $request)
    {
      $this->validator($request->json()->all())->validate();

      event(new Registered($user = $this->create($request->json()->all())));

      $this->guard()->login($user);

      return $this->registered($request, $user);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        return response()->json($user);
    }

    /**
     * Confirm email address
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    protected function confirm(Request $request)
    {
        $token = $request->route('token');
        if (is_null($token) || !$token) {
          return response()->json(['status' => 'No confirmation code to validate'], 404);
        }

        $user = User::where(['confirmation_code' => $token])->first();

        if (is_null($user) || !$user) {
          return response()->json(['status' => 'Token expired or invalid'], 404);
        }
        $user->update([
          'confirmed' => true,
          'confirmation_code' => null
        ]);

        return response()->json($user);
    }
}
