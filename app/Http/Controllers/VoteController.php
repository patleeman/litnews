<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class VoteController extends Controller
{
    public function vote (Request $request)
    {
      if (Auth::check()) {
        $this->validate($request, [
          'item_id' => 'required_without:comment_id|integer',
          'comment_id' => 'required_without:item_id|integer',
          'vote' => 'required|in:-1,1'
        ]);

        $user = Auth::user();

        $payload = [
          'item_id' => $request->input('item_id'),
          'comment_id' => $request->input('comment_id'),
          'user_id' => $user->id
        ];

        $vote = $request->input('vote');
        if ($vote == -1 || $vote == '-1') {
          if ($user->karma() >= 100) {
            $payload['vote'] = -1;
          } else {
            return response(422)->json(['err' => 'User can not downvote']);
          }
        } else {
          $payload['vote'] = 1;
        }

        $created = \App\Vote::firstOrCreate($payload);

        return response()->json($created);
      } else {
        return response(401);
      }
    }
}
