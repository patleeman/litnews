<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;

class ItemController extends Controller
{
    /**
     * Content for front page.
     * Uses a raw SQL select statement to bake in front page algo score.
     *
     */
    public function get (Request $request)
    {

      $page = (int) $request->input('page', 1);
      $limit = 30;
      $offset = ($page - 1) * $limit;

      $filter = $request->input('filter');
      if ($filter == 'new') {
        $orderby = 'created_at';
      } else {
        $orderby = 'score';
      }

      // $all_items = Cache::get('front_' . $orderby);
      // if (is_null($all_items)) {
      //   $all_items = \App\Item::all()->sortByDesc($orderby);
      //   Cache::put('front_' . $orderby, $all_items, 5);
      // }
      // $items = $all_items->splice($offset, $offset + $limit);

      $user = Auth::user();
      if (!is_null($user) && $user) {
        $items = DB::select("
            SELECT items.id, title, url, text, items.user_id, items.created_at, score.points, comments.num_comments, users.name as username, userVotes.vote as user_vote, count(saveds.id) > 0 as is_saved
              FROM items
              JOIN (
                SELECT items.id, sum(votes.vote) points, ((sum(votes.vote) - 1) / POWER((time_to_sec(timediff(Now(), items.`created_at`)) / 3600) + 2, 1.8)) score
                  FROM items
                  LEFT JOIN votes
                  ON items.id = votes.item_id
                  GROUP BY items.id
                ) score
              ON score.id = items.id
              JOIN (SELECT items.id, count(comments.id) num_comments FROM items LEFT JOIN comments ON items.id = comments.item_id GROUP BY items.id) comments
              ON comments.id = items.id
              JOIN users
              ON users.id = items.user_id
              LEFT JOIN ( SELECT id, item_id, vote FROM votes WHERE user_id='{$user->id}') userVotes
              ON userVotes.item_id = items.id
              LEFT JOIN (SELECT id, item_id, user_id FROM saveds WHERE user_id='{$user->id}') saveds
              ON saveds.item_id = items.id
              GROUP BY 1,2,3,4,5,6,7,8,9,10
              ORDER BY {$orderby} desc
              LIMIT :limit
              OFFSET :offset
            ", ['limit' => $limit, 'offset' => $offset]);
      } else {
        $items = DB::select("
            SELECT items.id, title, url, text, user_id, items.created_at, score.points, comments.num_comments, users.name as username, null as user_vote, null as is_saved
              FROM items
              JOIN (
                SELECT items.id, sum(votes.vote) points, ((sum(votes.vote) - 1) / POWER((time_to_sec(timediff(Now(), items.`created_at`)) / 3600) + 2, 1.8)) score
                  FROM items
                  LEFT JOIN votes
                  ON items.id = votes.item_id
                  GROUP BY items.id
                ) score
              ON score.id = items.id
              JOIN (SELECT items.id, count(comments.id) num_comments FROM items LEFT JOIN comments ON items.id = comments.item_id GROUP BY items.id) comments
              ON comments.id = items.id
              JOIN users
              ON users.id = items.user_id
              ORDER BY {$orderby} desc
              LIMIT :limit
              OFFSET :offset
            ", ['limit' => $limit, 'offset' => $offset]);
      }

      return response()->json($items);
    }

    /**
     * Get a single item
     */
    public function getItem ($id, Request $request)
    {
      $item = \App\Item::where('id', $id)->first();

      return response()->json($item);
    }

    /**
     * New item submission
     */
    public function create (Request $request)
    {
      if (Auth::check()) {
        $user = Auth::user();

        $this->validate($request, [
          'title' => 'required',
          'url' => 'required_without:text|url',
          'text' => 'required_without:url'
        ]);

        // Strip url params off url
        if (!is_null($request->input('url'))) {
          $url = strtok($request->input('url'), '?');
        } else {
          $url = null;
        }

        $newItem = \App\Item::create([
          'user_id' => $user->id,
          'title' => $request->input('title'),
          'url' => $url,
          'text' => $request->input('text')
        ]);

        \App\Vote::firstOrCreate([
          'item_id' => $newItem->id,
          'user_id' => $user->id,
          'vote' => 1
        ]);

        return response()->json($newItem);
      } else {
        return response(401);
      }
    }

    /**
     * Modification of a item. Only mods and admins should have access.
     */
    public function update ()
    {

    }

    /**
     * Hide a item.
     */
    public function delete ()
    {

    }
}
