<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function getUser ($username, Request $request) {

      $user = \App\User::where('name', $username)->first();
      $payload = [
        'name' => $user->name,
        'twitter' => $user->twitter,
        'about' => $user->about,
        'created_at' => $user->created_at->toIso8601String(),
        'karma' => $user->karma(),
        'submissions' => $user->items()->get()->toArray(),
        'comments' => $user->comments()->with('item')->get()->toArray()
      ];
      return response()->json($payload);
    }

    public function updateProfile (Request $request) {
      if (Auth::check()) {
        $user = Auth::user();

        $this->validate($request, [
          'email' => 'email',
          'password' => 'string|min:6|confirmed',
        ]);

        $data = $request->all();
        $allowed_update = ['email', 'twitter', 'about'];
        $payload = [];

        foreach ($allowed_update as $allowed) {
          if (array_key_exists($allowed, $data)) {
            if (is_null($data[$allowed])) {
              $data[$allowed] = '';
            }
            $payload[$allowed] = $data[$allowed];
          }
        }

        if ($request->input('password')) {
          $user->password = bcrypt($request->input('password'));
          $user->save();
        }

        $success = $user->update($payload);
        if ($success) {
          return response()->json($user);
        } else {
          return response(204);
        }

      }
    }
}
