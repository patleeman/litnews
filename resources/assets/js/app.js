import 'es6-promise/auto'
import vue from 'vue'
import router from './router'
import store from './store'
import App from './App.vue'
import VueAnalytics from 'vue-analytics'

window.Vue = vue;

Vue.use(VueAnalytics, {
  id: 'UA-104074689-1',
  router
})

const app = new Vue({
    el: '#app',
    router,
    store,
    components: { App },
    template:'<app />'
});
