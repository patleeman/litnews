const state = {
  isLoggedIn: false,
  name: '',
  email: '',
  twitter: '',
  about: '',
  karma: 0,
}

// getters
const getters = {
  // allProducts: state => state.all
}

// actions
const actions = {
  getUser ({commit}) {

  }
}

// mutations
const mutations = {
  SET_USER_STATUS (state, status) {
    state.isLoggedIn = status
  },

  SET_USER_INFO (state, data) {
    for (let key in data) {
      state[key] = data[key]
    }
  }
}


export default {
  state,
  getters,
  actions,
  mutations
}
