const state = {
  visible: false,
  body: '',
  type: 'is-info'
}

// getters
const getters = {
  // allProducts: state => state.all
}

// actions
const actions = {
  showNotification ({commit}, payload) {
    commit('SHOW_NOTIFICATION', payload)
  },
  hideNotification ({commit}) {
    commit('HIDE_NOTIFICATION')
  }
}

// mutations
const mutations = {
  SHOW_NOTIFICATION (state, payload) {
    state.body = payload.body
    if (payload.type != null && payload.type) {
      state.type = payload.type
    }
    state.visible = true
  },

  HIDE_NOTIFICATION (state) {
    state.visible = false
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
