const state = {
  flag: false,
  flagComment: {},
  flagItem: {}
}

// getters
const getters = {
  // allProducts: state => state.all
}

// actions
const actions = {
  setFlagModal ({ commit }, status) {
    commit('SET_FLAG', status)
  }
}

// mutations
const mutations = {
  SET_FLAG (state, status) {
    state.flag = status
  },
  SET_FLAG_COMMENT (state, comment) {
    state.flagComment = comment
  },
  SET_FLAG_ITEM (state, item) {
    state.flagItem = item
  }
}


export default {
  state,
  getters,
  actions,
  mutations
}
