import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'frontpage',
      component: require('../components/FrontPage.vue')
    },
    {
      path: '/new',
      name: 'new',
      component: require('../components/FrontPage.vue')
    },
    {
      path: '/i/:id',
      name: 'comments',
      component: require('../components/Comments.vue')
    },
    {
      path: '/c/:id',
      name: 'reply',
      component: require('../components/Reply.vue')
    },
    {
      path: '/u/:username',
      name: 'user',
      component: require('../components/User.vue')
    },
    {
      path: '/s',
      name: 'search',
      component: require('../components/Search.vue')
    },
    {
      path: '/profile',
      name: 'profile',
      component: require('../components/Profile.vue')
    },
    {
      path: '/submit',
      name: 'submit',
      component: require('../components/Submit.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: require('../components/Auth/Login.vue')
    },
    {
      path: '/register/verify/:token',
      name: 'verify',
      component: require('../components/Auth/Verify.vue')
    },
    {
      path: '/register',
      name: 'register',
      component: require('../components/Auth/Register.vue')
    },
    {
      path: '/password/reset',
      name: 'resetEmail',
      component: require('../components/Auth/RequestResetPassword.vue')
    },
    {
      path: '/password/reset/:token',
      name: 'resetToken',
      component: require('../components/Auth/ResetPassword.vue')
    },
    {
      path: '/privacypolicy',
      name: 'privacypolicy',
      component: require('../components/PrivacyPolicy.vue')
    },
    {
      path: '/terms',
      name: 'terms',
      component: require('../components/Terms.vue')
    },
    {
      path: '*',
      component: require('../components/404.vue')
    }
  ]
})
