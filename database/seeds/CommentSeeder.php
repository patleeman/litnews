<?php

use Illuminate\Database\Seeder;

class CommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Run the factory several times to get nested comments
        for ($i = 0; $i < 20; $i++) {
          factory(App\Comment::class, 100)->create();
        }
    }
}
