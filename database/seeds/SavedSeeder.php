<?php

use Illuminate\Database\Seeder;

class SavedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Saved::class, 10000)->create();
    }
}
