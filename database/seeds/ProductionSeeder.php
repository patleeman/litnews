<?php

use Illuminate\Database\Seeder;

class ProductionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        App\User::create([
          'name' => 'Admin',
          'email' => 'admin@lit-news.com',
          'password' => bcrypt('eP30yqVzXAyq'),
          'role' => 'admin'
        ]);

        App\User::create([
          'name' => 'LitNews',
          'email' => 'litnews@lit-news.com',
          'password' => bcrypt('7ShWX9h2xI1o'),
          'role' => 'admin'
        ]);

        App\User::create([
          'name' => 'Patrick',
          'email' => 'me@patricklee.nyc',
          'password' => bcrypt('Kbe5AObroz3A'),
          'role' => 'admin'
        ]);

        App\User::create([
          'name' => 'Abbe',
          'email' => 'Wrightabbe@gmail.com',
          'password' => bcrypt('Lx5cK42OVrdX'),
          'role' => 'admin'
        ]);

    }

}
