<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->unique()->username,
        'email' => $faker->unique()->safeEmail,
        'twitter' => $faker->username,
        'about' => $faker->paragraph(1),
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
        'created_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});

$factory->define(App\Item::class, function (Faker\Generator $faker) {
    $payload = [
      'title' => $faker->sentence(8, true),
      'user_id' => App\User::all()->random()->id,
      'created_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];

    if ($faker->numberBetween(1, 10) > 3) {
      $payload['url'] = $faker->url();
    } else {
      $payload['text'] = $faker->paragraph(3);
    }

    return $payload;
});

$factory->define(App\Comment::class, function (Faker\Generator $faker) {
    $targetLinkId = App\Item::all()->random()->id;

    $payload = [
        'item_id' => $targetLinkId,
        'user_id' => App\User::all()->random()->id,
        'comment' => $faker->text(500),
        'created_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];

    if ($faker->numberBetween(1, 10) > 3) {
      $comments = App\Comment::all();
      if (!is_null($comments) && count($comments) > 0) {
        $payload['parent_id'] = $comments->random()->id;
      }
    }

    return $payload;
});

$factory->define(App\Vote::class, function (Faker\Generator $faker) {
    $voteOptions = [-1, 1];
    $payload = [
        'user_id' => App\User::all()->random()->id,
        'vote' => $voteOptions[array_rand($voteOptions)],
        'created_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];

    if ($faker->numberBetween(1, 10) > 5) {
      $payload['item_id'] = App\Item::all()->random()->id;
    } else {
      $payload['comment_id'] = App\Comment::all()->random()->id;
    }

    return $payload;
});

$factory->define(App\Saved::class, function (Faker\Generator $faker) {
    $voteOptions = [-1, 1];
    $payload = [
        'user_id' => App\User::all()->random()->id,
        'created_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];

    if ($faker->numberBetween(1, 10) > 5) {
      $payload['item_id'] = App\Item::all()->random()->id;
    } else {
      $payload['comment_id'] = App\Comment::all()->random()->id;
    }

    return $payload;
});
