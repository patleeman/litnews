<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Auth and user endpoints
Route::group([], function () {
  $this->post('register/verify/{token}', 'Auth\RegisterController@confirm');
  $this->post('register', 'Auth\RegisterController@register');
  $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.reset');
  $this->post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.reset');
  $this->post('login', 'Auth\LoginController@login');
  $this->post('logout', 'Auth\LoginController@logout')->name('logout');
});

Route::group(['prefix' => '/api/v1', 'middleware' => ['throttle:60,1']], function () {
  $this->get('i', 'ItemController@get');
  $this->post('i', 'ItemController@create')->middleware('auth');
  $this->get('i/{id}', 'ItemController@getItem');
  $this->get('c/{id}', 'CommentController@getItem');
  $this->post('c', 'CommentController@addComment')->middleware('auth');
  $this->get('s/', 'SearchController@search');
  $this->get('u/{username}', 'UserController@getUser');
  $this->post('v', 'VoteController@vote')->middleware('auth');
  $this->get('saved', 'SavedController@get');
  $this->post('saved', 'SavedController@save')->middleware('auth');
  $this->post('saved/unsave', 'SavedController@unsave')->middleware('auth');
  $this->post('profile', 'UserController@updateProfile')->middleware('auth');
  $this->post('flag', 'ModController@flag')->middleware('auth');
});

Route::get('/', function () {
  if (Auth::check()) {
    $user_acct = Auth::user();
    $user = [
      'name' => $user_acct->name,
      'twitter' => $user_acct->twitter,
      'about' => $user_acct->about,
      'created_at' => $user_acct->created_at->toIso8601String(),
      'karma' => $user_acct->karma(),
      'submissions' => $user_acct->items()->get()->toArray(),
      'comments' => $user_acct->comments()->with('item')->get()->toArray()
    ];
    return view('app', compact('user'));
  } else {
    $user = null;
    return view('app', compact('user'));
  }
});

// Vue.js catchall
// https://laracasts.com/discuss/channels/laravel/vue-router-in-laravel-cant-go-directly-to-a-url
Route::get('/{vue_capture?}', function () {
    $user = Auth::user();
    return view('app', compact('user'));
})->where('vue_capture', '^((?!admin|api).)*$');
